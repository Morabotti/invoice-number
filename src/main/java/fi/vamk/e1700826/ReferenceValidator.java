package fi.vamk.e1700826;

import java.util.stream.IntStream;

public class ReferenceValidator {
    public static void main(String[] args) {
        System.out.println(createInvoiceNumber("123451232423132646534543543"));
    }

    public static String createInvoiceNumber(String number) {
        if (number.length() <= 3 || number.length() >= 20) {
            return null;
        }

        int[] series = new int[3];
        series[2] = 7;
        series[1] = 3;
        series[0] = 1;

        int sumOfMultiplication = 0;

        for(int i = 0; i < number.length() ; i++) {
            int pos = Integer.parseInt(String.valueOf(number.charAt(i)));
            sumOfMultiplication += pos * series[i % 3];
        }

        int roundUp = ((sumOfMultiplication - 1) / 10 + 1) * 10;
        int referenceNumber = roundUp - sumOfMultiplication;
        String invoice = "" + number + "" + referenceNumber;

        System.out.println(invoice);
        StringBuilder output = new StringBuilder();

        for (int i = invoice.length() - 1; i >= 0; i--) {
            boolean shouldSpace = (invoice.length() - i) % 5 == 0;
            output.append(shouldSpace ? invoice.charAt(i) + " " : invoice.charAt(i));
        }

        return output.reverse().toString().trim();
    }
}
