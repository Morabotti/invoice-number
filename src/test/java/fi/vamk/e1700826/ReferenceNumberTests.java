package fi.vamk.e1700826;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ReferenceNumberTests {
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    public void shouldBeMoreThanThree() {
        String invoice = ReferenceValidator.createInvoiceNumber("23");
        assertNull(invoice);
    }

    @Test
    public void shouldBeLessThanTwenty() {
        String invoice = ReferenceValidator.createInvoiceNumber("1234567890123456789012");
        assertNull(invoice);
    }

    @Test
    public void isCorrectFormat() {
        String invoice = ReferenceValidator.createInvoiceNumber("12345");
        assertEquals(invoice, "1 23453");
    }

    @Test
    public void isCorrectFormatting() {
        String invoice = ReferenceValidator.createInvoiceNumber("1234567890123");
        assertEquals(invoice, "1234 56789 01237");
    }

    @Test
    public void isCorrectNumbers() {
        String invoice = ReferenceValidator.createInvoiceNumber("1234");
        assertEquals(invoice, "12348");
    }
}
